﻿namespace DocuRISE.Data.Seeding.Contracts
{
    public interface ISeeder
    {
        Task Seed(ApplicationDbContext data, IServiceProvider serviceProvider);
    }
}
