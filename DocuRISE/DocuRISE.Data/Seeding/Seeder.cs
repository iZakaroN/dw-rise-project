﻿using DocuRISE.Data.Seeding.Contracts;

namespace DocuRISE.Data.Seeding
{
    public class Seeder : ISeeder
    {
        public async Task Seed(ApplicationDbContext data, IServiceProvider serviceProvider)
        {
            var seeders = new List<ISeeder>()
                {
                    new RolesSeeder(),
                    new UsersSeeder()
                };

            foreach (var seeder in seeders)
            {
                await seeder.Seed(data, serviceProvider);
            }
        }
    }
}
