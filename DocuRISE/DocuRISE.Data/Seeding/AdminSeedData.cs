﻿namespace DocuRISE.Data.Seeding
{
    public class AdminSeedData
    {
        public const string AdminFirstName = "Nicoleta";
        public const string AdminLastName = "Valcheva";
        public const string AdminCompanyName = "Nemetschek Bulgaria";
        public const string AdminEmail = "fmAdmin@mail.bg";
        public const string AdminPassword = "Admin123*";
    }
}
