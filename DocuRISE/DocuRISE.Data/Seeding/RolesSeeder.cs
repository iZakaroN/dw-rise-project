﻿using DocuRISE.Data.Seeding.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using static DocuRISE.Common.GlobalConstants;

namespace DocuRISE.Data.Seeding
{
    public class RolesSeeder : ISeeder
    {
        public async Task Seed(ApplicationDbContext data, IServiceProvider serviceProvider)
        {
            var roleManager =
                serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            if (await roleManager.RoleExistsAsync(FacilityManagerAdministratorRoleName))
            {
                return;
            }

            var adminRole = new IdentityRole { Name = FacilityManagerAdministratorRoleName };

            await roleManager.CreateAsync(adminRole);

            if (await roleManager.RoleExistsAsync(CompanyAdministratorRoleName))
            {
                return;
            }

            var companyAdminRole = new IdentityRole { Name = CompanyAdministratorRoleName };

            await roleManager.CreateAsync(companyAdminRole);

            if (await roleManager.RoleExistsAsync(EmployeeRoleName))
            {
                return;
            }

            var employeeRole = new IdentityRole { Name = EmployeeRoleName };

            await roleManager.CreateAsync(employeeRole);

            await data.SaveChangesAsync();
        }
    }
}
