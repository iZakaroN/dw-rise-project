﻿using DocuRISE.Data.Models;
using DocuRISE.Data.Seeding.Contracts;
using DocuRISE.Shared.Models.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using static DocuRISE.Common.GlobalConstants;

namespace DocuRISE.Data.Seeding
{
    public class UsersSeeder : ISeeder
    {
        public async Task Seed(ApplicationDbContext data, IServiceProvider serviceProvider)
        {
            var userManager =
                serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            if (!data.Roles.Any(r => r.Name == FacilityManagerAdministratorRoleName))
            {
                await SeedAdmin(userManager);
            }

            if (!data.Users.Any(u => u.Email == AdminSeedData.AdminEmail))
            {
                await SeedAdmin(userManager);
            }

            await data.SaveChangesAsync();
        }

        private static async Task SeedAdmin(UserManager<ApplicationUser> userManager)
        {
            var admin = new ApplicationUser()
            {
                FirstName = AdminSeedData.AdminFirstName,
                LastName = AdminSeedData.AdminLastName,
                //CompanyName = AdminSeedData.AdminCompanyName,
                Email = AdminSeedData.AdminEmail,
                UserName = AdminSeedData.AdminEmail
            };

            await userManager.CreateAsync(admin, AdminSeedData.AdminPassword);

            await userManager.AddToRoleAsync(admin, FacilityManagerAdministratorRoleName);
        }
    }
}
