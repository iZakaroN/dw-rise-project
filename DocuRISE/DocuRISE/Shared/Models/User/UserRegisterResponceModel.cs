﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuRISE.Shared.Models.User
{
    public class UserRegisterResponceModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string CompanyName { get; set; }
    }
}
