﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocuRISE.Shared.Models.Document;

namespace DocuRISE.Shared.Models.User
{
    public class UsersViewServiceModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public ICollection<FileListingModel> Documents { get; set; }
    }
}
