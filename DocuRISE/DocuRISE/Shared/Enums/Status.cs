﻿namespace DocuRISE.Shared.Enums
{
    public enum Status
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3,
        Deleted = 4
    }
}
