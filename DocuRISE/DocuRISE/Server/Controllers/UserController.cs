﻿using DocuRISE.Data;
using DocuRISE.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DocuRISE.Shared.Models.User;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace DocuRISE.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext data;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IConfiguration _configuration;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserController(ApplicationDbContext data, UserManager<ApplicationUser> userManager,
            IConfiguration configuration, SignInManager<ApplicationUser> signInManager)
        {
            this.data = data;
            this._userManager = userManager;

            _configuration = configuration;
            _signInManager = signInManager;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] UserRegisterRequestModel model)
        {
            var company = await data.Companies.FirstOrDefaultAsync(c => c.Name == model.CompanyName);
            if (company == null)
            {
                company = new Company()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = model.CompanyName
                };
                await data.Companies.AddAsync(company);
                await data.SaveChangesAsync();
            }

            var newUser = new ApplicationUser { FirstName = model.FirstName, LastName = model.LastName, Email = model.Email, UserName = model.Email, CompanyId = company.Id, Company = company};

            var result = await _userManager.CreateAsync(newUser, model.Password);
            await _userManager.AddToRoleAsync(newUser, data.Roles.Where(r => r.Id == model.RoleId).FirstOrDefault().ToString());
            
            await data.SaveChangesAsync();

            if (!result.Succeeded)
            {
                var errors = result.Errors.Select(x => x.Description);

                return Ok(new RegisterResult { Successful = false, Errors = errors });

            }

            return Ok(new RegisterResult { Successful = true });
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginRequestModel login)
        {
            var result = await _signInManager.PasswordSignInAsync(login.Email, login.Password, false, false);

            if (!result.Succeeded) return BadRequest(new LoginResult { Successful = false, Error = "Email or password are invalid." });

            var user = await _signInManager.UserManager.FindByEmailAsync(login.Email);
            var roles = await _signInManager.UserManager.GetRolesAsync(user);
            var claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Name, login.Email));

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecurityKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiry = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JwtExpiryInDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtAudience"],
                claims,
                expires: expiry,
                signingCredentials: creds
            );

            return Ok(new LoginResult { Successful = true, Token = new JwtSecurityTokenHandler().WriteToken(token) });
        }

        [HttpGet]
        [Route("getRoles")]
        [AllowAnonymous]
        public async Task<IEnumerable<RoleServiceModel>> GetRoles()
        {
            var roles = await data
                .Roles
                .Select(r => new RoleServiceModel()
                {
                    Id = r.Id,
                    Name = r.Name,
                })
                .ToListAsync();

            return roles;
        }

        public async Task<string> GetRoleById(string id)
        {
            var roleName = (await data.Roles.FirstOrDefaultAsync(r => r.Id == id)).Name;
            return roleName;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("getAllUsers")]

        public async Task<List<UsersViewServiceModel>> GetAllUsers()
        {
            var users = await data
                .Users
                .Select(u => new UsersViewServiceModel()
                    {
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        CompanyName = u.Company.Name,
                    })
                .ToListAsync();

            return users;
        }
    }
}
