﻿using DocuRISE.Data;
using DocuRISE.Data.Models;
using DocuRISE.Shared.Models.Document;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace DocuRISE.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class FileController : ControllerBase
    {
        private readonly ApplicationDbContext data;
        private readonly IWebHostEnvironment env;

        public FileController(ApplicationDbContext data, IWebHostEnvironment env)
        {
            this.data = data;
            this.env = env;
        }

        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<string> UploadFile([FromForm] IFormFile file)
        {
            if (file != null)
            {
                try
                {
                    var rootPath = env.ContentRootPath;
                    string trustedFileName = Path.GetRandomFileName();
                    var saveLocation = Path.Combine(rootPath, "SavedFiles", trustedFileName);
                    using (var fileStream = new FileStream(saveLocation, FileMode.CreateNew))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    var shortenedPath = Path.Combine("SavedFiles", trustedFileName);
                    return shortenedPath;
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 500;
                    await Response.WriteAsync("Upload failed");

                }
            }

            return string.Empty;
        }


        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<IActionResult> UploadToDb(FileUploadModel document)
        {
            if (document == null)
            {
                return BadRequest("A problem occured with document.");
            }

            var company = await data
                .Companies
                .FirstOrDefaultAsync(c => c.Name == document.CompanyName);

            if (company == null)
            {
                return BadRequest($"Company does not exist.");
            }

            var newDocument = new Document()
            {
                Company = company,
                CompanyId = company.Id,
                Name = document.FileName,
                Path = document.Path,
                CreatedOn = DateTime.Now,
                DocumentTypeId = document.DocumentTypeId,
            };

            await data.Documents.AddAsync(newDocument);
            var response = await data.SaveChangesAsync();
            return Ok(response);
        }


        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> RemoveFile(string filePath)
        {
            if (filePath != null)
            {
                try
                {
                    var rootPath = env.ContentRootPath;
                    var fileLocation = Path.Combine(rootPath, filePath);

                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 500;
                    await Response.WriteAsync("Remove Failed");
                }
            }

            return new OkResult();
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("getDocumentType")]
        public async Task<IEnumerable<DocumentTypeServiceModel>> GetDocumentType()
        {
            var types = await data
                .DocumentTypes
                .Select(t => new DocumentTypeServiceModel()
                {
                    Id = t.Id,
                    Name = t.Name,
                }).ToListAsync();

            return types;
        }
        [HttpGet]
        [Route("[action]")]
        [AllowAnonymous]
        public async Task<List<FileListingModel>> GetAll()
        {
            var files = await data
                .Documents
                .Select(f => new FileListingModel()
                {
                    Id = f.Id,
                    FileName = f.Name,
                    CompanyName = f.Company.Name,
                    DocumentType = f.DocumentType.Name,
                    Status = f.Status.ToString()
                })
                .ToListAsync();

            return files;
        }

        [HttpGet("{fileName}")]
        [AllowAnonymous]
        public async Task<IActionResult> DownloadFile(string fileName)
        {
            var returnedFile = data.Documents.FirstOrDefault(x => x.Name.Equals(fileName));

            var path = Path.Combine(env.ContentRootPath, returnedFile.Path);

            var memoryStream = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0;
            return File(memoryStream, MediaTypeNames.Application.Pdf, Path.GetFileName(path));
        }
    }
}
