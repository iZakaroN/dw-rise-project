﻿using DocuRISE.Data;
using DocuRISE.Data.Seeding;
using Microsoft.EntityFrameworkCore;

namespace DocuRISE.Server.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder PrepareDatabase(this IApplicationBuilder app)
        {
            using var scopedServices = app.ApplicationServices.CreateScope();
            var services = scopedServices.ServiceProvider;
            var data = scopedServices.ServiceProvider
                .GetService<ApplicationDbContext>();

            //data.Database.Migrate();

            var seeder = new Seeder();
            seeder.Seed(data, services);

            return app;
        }
    }
}
