﻿using System.Security.Claims;
using static DocuRISE.Common.GlobalConstants;

namespace DocuRISE.Server.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetId(this ClaimsPrincipal user)
        {
            return user.FindFirst(ClaimTypes.NameIdentifier).Value;
        }

        public static bool IsAdmin(this ClaimsPrincipal user)
        {
            return user.IsInRole(FacilityManagerAdministratorRoleName);
        }

        public static bool IsCompanyAdmin(this ClaimsPrincipal user)
        {
            return user.IsInRole(CompanyAdministratorRoleName);
        }

        public static bool IsEmployee(this ClaimsPrincipal user)
        {
            return user.IsInRole(EmployeeRoleName);
        }
    }
}
