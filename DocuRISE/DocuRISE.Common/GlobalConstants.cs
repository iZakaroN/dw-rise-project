﻿namespace DocuRISE.Common
{
    public class GlobalConstants
    {
        public const string FacilityManagerAdministratorRoleName = "Administrator";

        public const string CompanyAdministratorRoleName = "CompanyAdministrator";

        public const string EmployeeRoleName = "Employee";
    }
}
